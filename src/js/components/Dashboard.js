import $ from 'jquery';
import Spinner from './Spinner';
import LoadMore from './LoadMore';

export default class Dashboard {
  constructor( dashboard ) {
    if (!dashboard) return;

    this.dashboard = dashboard;
    this.contentButtons = this.dashboard.querySelectorAll('[data-dashboard-content]');
    this.header = this.dashboard.querySelector('[data-dashboard-title]');
    this.feed = this.dashboard.querySelector('[data-dashboard-feed]');

    this.content = this.contentButtons[0].dataset.dashboardContent;
    this.page = 1;
    this.hasMore = true;
    this.loading = false;

    this.changeContent = this.changeContent.bind(this);
    this.fetchContent = this.fetchContent.bind(this);
    this.setLoading = this.setLoading.bind(this);
  }

  init() {
    if (this.contentButtons) {
      this.contentButtons.forEach(button => {
        button.addEventListener('click', this.changeContent);
      });
    }

    if (this.dashboard) {
      this.dashboard.addEventListener('contentTypeChanged', e => this.fetchContent());
      this.fetchContent();
    }
  }

  changeContent(e) {
    this.contentButtons.forEach(button => button.classList.remove('active'));
    e.target.classList.add('active');

    this.content = e.target.dataset.dashboardContent;
    this.header.innerText = alkitabCourseGrid.dashboardHeaders[this.content];
    this.page = 1;
    this.feed.innerHTML = '';
    this.more = true;

    let event = new Event('contentTypeChanged');

    this.dashboard.dispatchEvent(event);
  }

  fetchContent() {
    this.showLoadMore(false);
    this.setLoading(true);
    $.get({
      url: alkitabCourseGrid.ajaxUrl,
      data: {
        type: this.content,
        page: this.page,
        action: 'dashboard'
      },
      success: (response) => {
        this.setLoading(false);
        if (response.content) {
          this.feed.innerHTML = this.feed.innerHTML + response.content;
          this.page = this.page + 1;
          this.more = this.page <= response.pages;
          if (this.more) {
            this.showLoadMore(true);
          }
        }
      },
      error: (e) => {
        console.log(e);
        this.setLoading(false);
      }
    })
  }

  showLoadMore(show) {
    if (show) {
      let loadmore = LoadMore(() => {
        this.fetchContent();
      });
      this.feed.appendChild(loadmore);
    } else {
      let loadmore = this.feed.querySelector('[data-loadmore]');
      if (loadmore) loadmore.remove();
    }
  }

  setLoading(loading) {
    let spinner = Spinner();

    if (loading) {
      this.loading = loading;
      this.feed.appendChild(spinner);
    } else {
      let s = this.feed.querySelector('.la-ball-clip-rotate');
      if (s) {
        s.closest('.row-center').remove();
      }
    }
  }
}