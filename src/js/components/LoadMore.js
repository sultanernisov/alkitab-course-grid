export default (cb) => {
  let button = document.createElement('button');
  button.addEventListener('click', cb, {
    once: true,
  });
  button.classList.add('c-dashboard--loadmore');
  button.setAttribute('data-loadmore', 
  '');
  button.innerText = alkitabCourseGrid.strings.loadmore;
  return button;
};