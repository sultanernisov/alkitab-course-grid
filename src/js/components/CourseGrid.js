import $ from 'jquery';
import LoadMore from './LoadMore';
import Spinner from './Spinner';

export default class CourseGrid {
  constructor(element) {
    if (!element) return;

    // elements
    this.grid = element;
    this.filtersBody = this.grid.querySelector('[data-filters]');
    this.filtersToggle = this.grid.querySelector('[data-filters-toggle]');
    this.filterItems = this.grid.querySelectorAll('[data-type]');
    this.feed = this.grid.querySelector('[data-course-feed]');

    // state
    this.page = 1;
    this.filters = [];
    this.loading = false;
    this.more = true;

    // methods
    this.toggleFilters = this.toggleFilters.bind(this);
    this.fetch = this.fetch.bind(this);
    this.onFilterClick = this.onFilterClick.bind(this);
    this.showLoadMore = this.showLoadMore.bind(this);
    this.setLoading = this.setLoading.bind(this);
  }

  init() {
    if (!this.grid) return;
    this.filtersToggle.addEventListener('click', this.toggleFilters);
    this.filterItems.forEach(item => {
      item.addEventListener('click', this.onFilterClick);
    });
  }

  fetch() {
    this.showLoadMore(false);
    this.setLoading(true);
    $.get({
      url: alkitabCourseGrid.ajaxUrl,
      data: {
        action: 'alkitab_course_grid',
        page: this.page,
        cat: this.filters.filter(f => f.type === 'cat').map(f => f.slug).join(','),
        tag: this.filters.filter(f => f.type === 'tag').map(f => f.slug).join(',')
      },
      success: (response) => {
        this.setLoading(false);
        this.feed.innerHTML = this.feed.innerHTML + response.courses.join('');
        this.page = this.page + 1;
        this.more = this.page <= response.max_num_pages;
        if (this.more) {
          this.showLoadMore(true);
        }
      },
      error: (error) => {
        this.setLoading(false);
        console.log(error);
      }
    })
  }

  toggleFilters(e) {
    this.filtersBody.classList.toggle('open');
  }

  onFilterClick(e) {
    let { type, slug, selected } = e.currentTarget.dataset;
    if (selected === 'true') {
      e.currentTarget.dataset.selected = 'false';
      this.filters = this.filters.filter(filter => filter.slug !== slug);
    } else {
      e.currentTarget.dataset.selected = 'true';
      this.filters.push({ type, slug });
    }

    this.feed.innerHTML = '';
    this.page = 1;
    this.more = true;
    this.fetch();
  }

  showLoadMore(show) {
    if (show) {
      let loadmore = LoadMore(() => {
        this.fetch();
      });
      this.feed.appendChild(loadmore);
    } else {
      let loadmore = this.feed.querySelector('[data-loadmore]');
      if (loadmore) loadmore.remove();
    }
  }

  setLoading(loading) {
    let spinner = Spinner();
    this.loading = loading;

    if (loading) {
      this.feed.appendChild(spinner);
    } else {
      let s = this.feed.querySelector('.la-ball-clip-rotate');
      if (s) {
        s.closest('.row-center').remove();
      }
    }
  }
}