import $ from 'jquery';
import Dashboard from './components/Dashboard';
import CourseGrid from './components/CourseGrid';

$(document).ready(() => {
  let dashboard = new Dashboard(document.querySelector('[data-alkitab-dashboard]'));
  dashboard.init();

  let courseGrid = new CourseGrid(document.querySelector('[data-course-grid]'));
  courseGrid.init();
});