<div class="c-dashboard" data-alkitab-dashboard>

  <div class="c-dashboard-menu">
    <button
      class="c-dashboard-menu--item active"
      data-dashboard-content="active"
    >
      <?= __( 'Active courses', 'alkitab-course-grid' ); ?>
    </button>
    <button
      class="c-dashboard-menu--item"
      data-dashboard-content="completed"
    >
      <?= __( 'Completed courses', 'alkitab-course-grid' ); ?>
    </button>
    <button
      class="c-dashboard-menu--item"
      data-dashboard-content="certificates"
    >
      <?= __( 'Certificates', 'alkitab-course-grid' ); ?>
    </button>
  </div>

  <div class="c-dashboard--content">
    <h1
      class="c-dashboard--title"
      data-dashboard-title
    >
      <?= __( 'Active Courses', 'alkitab-course-grid' ); ?>
    </h1>
    <div class="c-dashboard-feed" data-dashboard-feed></div>
  </div>

</div>