<?php
  $course_id = get_the_id();
  $categories = wp_get_post_terms( $course_id, 'ld_course_category' );
  $tags = wp_get_post_terms( $course_id, 'ld_course_tag' );
?>

<div class="c-course-card">
  <?php
    if (has_post_thumbnail()) {
      the_post_thumbnail( 'thumbnail', array( 'class' => 'c-course-card--thumbnail' ) );
    }
  ?>
  <div class="c-course-card--details">
    <span class="c-course-card--title"><?php the_title(); ?></span>

    <?php
      if ( is_user_logged_in() && in_array( $course_id, ld_get_mycourses( get_current_user_id() ) ) ):
        $steps_total = learndash_get_course_steps_count( get_the_id() );
        $steps_completed = learndash_course_get_completed_steps( get_current_user_id(), get_the_id() );

        $progress = ($steps_total == 0) ? 0 : $steps_completed / $steps_total * 100;
    ?>
      <div class="c-course-card--progress-bar">
        <div class="c-course-card--progress" style="width: <?= $progress; ?>%"></div>
      </div>
    <?php endif; ?>

    <div class="c-course-card--terms">
      <?php if ( count( $categories ) > 0 ): ?>
        <span class="c-course-card--categories">
          <i class="c-course-card--term-icon" data-feather="folder"></i>
          <?php
            foreach ( array_slice( $categories, 0, 2 ) as $cat ) {
              echo '<a href="' . get_category_link( $cat->term_id ) . '">' . $cat->name . '</a>';
            }
          ?>
        </span>
      <?php endif; ?>

      <?php if ( count( $tags ) > 0 ): ?>
        <span class="c-course-card--tags">
          <i class="c-course-card--term-icon" data-feather="tag"></i>
          <?php
            foreach ( array_slice( $tags, 0, 2 ) as $tag ) {
              echo '<a href="' . get_category_link( $tag->term_id ) . '">' . $tag->name . '</a>';
            }
          ?>
        </span>
      <?php endif; ?>
    </div>

    <a
      class="c-course-card--link"
      href="<?= the_permalink(); ?>"
    >
      <?= __( 'Go to course', 'alkitab-course-grid' ); ?>
    </a>
  </div>
</div>