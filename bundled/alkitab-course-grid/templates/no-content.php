<?php if ( get_theme_mod( 'alkitab_explore_page_link' ) ): ?>
  <div>
    <p><?= __( 'You have not enrolled any courses.', 'alkitab-course-grid' ); ?></p>
    <a href="<?= get_permalink( get_theme_mod( 'alkitab_explore_page_link' ) ); ?>">
      <?= __( 'Would you like to see our courses?', 'alkitab-course-grid' ); ?>
    </a>
  </div>
<?php endif;