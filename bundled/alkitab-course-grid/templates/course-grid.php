<div class="c-course-grid" data-course-grid>
  <div class="c-course-grid--filters">
    <div class="c-course-grid--filters--header">
      <p class="c-course-grid--filters--title"><?= __( 'Refine your search', 'alkitab-course-grid' ); ?></p>
      <span data-filters-toggle>
        <i data-feather="sliders" class="c-course-grid--filter-icon"></i>
      </span>
    </div>
    <div class="c-course-grid--filters--body" data-filters>
      <div class="c-course-grid--filters--categories">
        <h6><?= __( 'Categories', 'alkitab-course-grid' ); ?></h6>
        <?php foreach ( $env['categories'] as $category ):?>
          <button
            class="c-course-grid--filters--category"
            data-selected="false"
            data-type="cat"
            data-slug="<?= $category->slug; ?>"
          >
            <?= $category->name; ?>
          </button>
        <?php endforeach; ?>
      </div>
      <div class="c-course-grid--filters--categories">
        <h6><?= __( 'Tags', 'alkitab-course-grid' ); ?></h6>
        <?php foreach ( $env['tags'] as $tag ):?>
          <button
            class="c-course-grid--filters--category"
            data-selected="false"
            data-type="tag"
            data-slug="<?= $tag->slug; ?>"
          >
            <?= $tag->name; ?>
          </button>
        <?php endforeach; ?>
      </div>
    </div>
  </div>

  <div class="c-course-grid--feed" data-course-feed>
    <?php
      foreach ( $env['courses'] as $course ) {
        echo $course;
      }
    ?>
  </div>
</div>