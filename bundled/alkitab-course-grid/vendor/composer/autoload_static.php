<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit4fa33b171cf670db32e4ed37f9542df7
{
    public static $prefixLengthsPsr4 = array (
        'A' => 
        array (
            'AlkitabCourseGrid\\' => 18,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'AlkitabCourseGrid\\' => 
        array (
            0 => __DIR__ . '/../..' . '/inc',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit4fa33b171cf670db32e4ed37f9542df7::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit4fa33b171cf670db32e4ed37f9542df7::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
