<?php

namespace AlkitabCourseGrid\Base;

class RenderComponent {

  protected function get_template_html( $tag, $env = array() ) {
    ob_start();
    require ALKITAB_COURSE_GRID_PLUGIN_PATH . "/templates/$tag.php";
    $html = ob_get_contents();
    ob_end_clean();
    
    return $html;
  }

}