<?php

namespace AlkitabCourseGrid\Base;

class Enqueue {

  public function register() {
    add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
  }

  public function enqueue_scripts() {

    wp_register_script( 'alkitab-course-grid', ALKITAB_COURSE_GRID_PLUGIN_URI . 'assets/js/main.js', array( 'jquery' ), false, true );

    wp_localize_script( 'alkitab-course-grid', 'alkitabCourseGrid', $this->get_script_localization() );

    wp_enqueue_script( 'alkitab-course-grid' );

  }

  private function get_script_localization() {
    return [
      'ajaxUrl' => admin_url( 'admin-ajax.php' ),
      'dashboardHeaders' => [
        'active' => __( 'Active Courses', 'alkitab-course-grid' ),
        'completed' => __( 'Completed Courses', 'alkitab-course-grid' ),
        'certificates' => __( 'Certificates', 'alkitab-course-grid' ),
      ],
      'strings' => [
        'loadmore' => __( 'Load More', 'alkitab-course-grid' ),
      ]
    ];
  }

}