<?php

namespace AlkitabCourseGrid;

class Init {

  public static function register_services() {
    foreach ( self::get_services() as $class ) {
      $service = self::instantiate( $class );

      if ( method_exists( $service, 'register' ) ) {
        $service->register();
      }
    }
  }

  public static function get_services() {
    return array(
      Base\Enqueue::class,
      Shortcodes\Dashboard::class,
      Shortcodes\CourseGrid::class
    );
  }

  public static function instantiate( $class ) {
    return new $class;
  }

}