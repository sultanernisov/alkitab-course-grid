<?php

namespace AlkitabCourseGrid\Shortcodes;
use \AlkitabCourseGrid\Base\RenderComponent;

class Dashboard extends RenderComponent {

  public function register() {
    add_shortcode( 'alkitab-user-dashboard', array( $this, 'render' ) );
    add_action( 'wp_ajax_dashboard', array( $this, 'ajax' ) );
  }

  public function render() {
    return $this->get_template_html( 'dashboard' );
  }

  public function ajax() {
    if ( $_SERVER['REQUEST_METHOD'] == 'GET' ) {
      $content_type = $_GET['type'];
      $page = $_GET['page'];
      $user_id = get_current_user_id();

      switch ($content_type) {
        case 'active':
          $courses = ld_get_mycourses( $user_id );
          $courses = array_filter( $courses, function($course_id) {
            return in_array( learndash_course_status( $course_id, $user_id, true ), ['not-started', 'in-progress'] );
          });
          $response = $this->get_courses( $courses, $page );
          $response['type'] = 'active';
          wp_send_json( $response );
          break;

        case 'completed':
          $courses = ld_get_mycourses( $user_id );
          $courses = array_filter( $courses, function($course_id) use ($user_id) {
            return learndash_course_status( $course_id, $user_id, true ) == 'completed';
          });

          $response = $this->get_courses( $courses, $page );
          $response['type'] = 'completed';
          wp_send_json($response);
          break;

        // TODO: create a certificate and see
        case 'certificates':
          if ( shortcode_exists( 'uo_learndash_certificates' ) ) {
            echo do_shortcode('[uo_learndash_certificates]');
          }
          break;

        default:
          break;
      }
    }
    wp_die();
  }

  private function get_courses( $courses, $page ) {
    if ( count($courses) < 1 ) return [
      'content' => $this->get_template_html( 'no-content' )
    ];

    $args = [
      'post_type' => 'sfwd-courses',
      'post__in' => $courses,
      'paged' => $page
    ];

    $query = new \WP_Query( $args );
    $content = '';
    $max_num_pages = $query->max_num_pages;

    if ( $query->have_posts() ) {
      while ( $query->have_posts() ) {
        $query->the_post();
        $content .= $this->get_template_html( 'course-card', $atts );
      }
    }

    wp_reset_postdata();

    return [
      'content' => $content,
      'pages' => $max_num_pages
    ];
  }

}