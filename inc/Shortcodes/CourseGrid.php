<?php

namespace AlkitabCourseGrid\Shortcodes;
use AlkitabCourseGrid\Base\RenderComponent;

class CourseGrid extends RenderComponent {

  public function register() {
    add_shortcode( 'alkitab-course-grid', array( $this, 'render' ) );
    add_action( 'wp_ajax_alkitab_course_grid', array( $this, 'ajax' ) );
    add_action( 'wp_ajax_nopriv_alkitab_course_grid', array( $this, 'ajax' ) );
  }

  public function render() {
    $attributes = [
      'categories' => $this->get_categories(),
      'tags' => $this->get_tags()
    ];

    $query = new \WP_Query( $this->get_filters() );
    $attributes['courses'] = $this->get_courses( $query );
    $attributes['max_num_pages'] = $query->max_num_pages;

    return $this->get_template_html( 'course-grid', $attributes );
  }

  public function ajax() {
    if ( $_SERVER['REQUEST_METHOD'] == 'GET' ) {
      $query = new \WP_Query($this->get_filters());
      $response = [
        'courses' => $this->get_courses( $query ),
        'max_num_pages' => $query->max_num_pages,
      ];

      wp_send_json( $response );
    }
  }

  /**
   * Get list of all categories available
   */
  private function get_categories() {
    return get_categories([
      'taxonomy' => 'ld_course_category',
      'hide_empty' => true
    ]);
  }

  /**
   * Get list of all tags available
   */
  private function get_tags() {
    return get_terms([
      'taxonomy' => 'ld_course_tag',
      'hide_empty' => true
    ]);
  }

  /**
   * Get list of courses from query
   */
  private function get_courses( $query = null ) {
    if ( !isset( $query ) ) return;
    $courses = [];

    if ( $query->have_posts() ) {
      while ( $query->have_posts() ) {
        $query->the_post();
        $courses[] = $this->get_template_html( 'course-card' );
      }
    }

    wp_reset_postdata();
    return $courses;
  }

  /**
   * forms query arguments from GET request
   */
  private function get_filters() {
    if ( $_SERVER['REQUEST_METHOD'] == 'GET' ) {
      $category_in = !empty($_GET['cat']) ? explode( ',', $_GET['cat'] ) : null;
      $tags_related = !empty($_GET['tag']) ? explode( ',', $_GET['tag'] ) : null;
      $search_query = get_search_query();
      $page = isset($_GET['page']) ? intval($_GET['page']) : 1;

      $args = [
        'post_type' => 'sfwd-courses',
        'paged' => $page
      ];

      if ( isset( $category_in ) ) {
        $args['tax_query'][] = [
          'taxonomy' => 'ld_course_category',
          'field' => 'slug',
          'terms' => $category_in
        ];
      }

      if ( isset( $tags_related ) ) {
        $args['tax_query'][] = [
          'taxonomy' => 'ld_course_tag',
          'field' => 'slug',
          'terms' => $tags_related
        ];
      }

      if ( isset($search_query) ) {
        $args['s'] = $search_query;
      }

      return $args;
    }
  }


}