<?php
/**
 * PLugin Name: Alkitab Course Grid
 * Description: Plugin adds course grids
 * Version: 0.0.2
 * Text Domain: alkitab-course-grid
 */

if ( !defined( 'ABSPATH' ) ) die;

if ( file_exists( dirname( __FILE__ ) . '/vendor/autoload.php' ) ) {
  require_once dirname( __FILE__ ) . '/vendor/autoload.php';
}

define( 'ALKITAB_COURSE_GRID_PLUGIN_PATH', plugin_dir_path( __FILE__ ) );
define( 'ALKITAB_COURSE_GRID_PLUGIN_URI', plugin_dir_url( __FILE__ ) );
define( 'ALKITAB_COURSE_GRID_PLUGIN', plugin_basename( __FILE__ ) );

function activate_alkitab_course_grid_plugin() {
  AlkitabCourseGrid\Base\Activate::activate();
}

function deactivate_alkitab_course_grid_plugin() {
  AlkitabCourseGrid\Base\Deactivate::deactivate();
}

register_activation_hook( __FILE__, 'activate_alkitab_course_grid_plugin' );
register_deactivation_hook( __FILE__, 'deactivate_alkitab_course_grid_plugin' );

if ( class_exists( 'AlkitabCourseGrid\\Init' ) ) {
  AlkitabCourseGrid\Init::register_services();
}